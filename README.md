# README #

This repository contains java programs for advance data structure and algorithms
### List of Programs ###

* Linear Search
* Binary Search
* Bubble Sort
* Selection Sort
* Insertion Sort

### How do I get set up? ###

* Install JDK 8+
* Install Intellij and Setup SDK in Project Structure
* Clone repository using HTTPS url from bitbucket

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
