package advance.dsa.recursion;

public class Fibonacci {

    //Head Recursion
    public static int fib_head_recursion(int term){
        if(term <=1 ) // base condition
            return term;

        //head recursion as last statement is not a standalone recursive function call
        return fib_head_recursion(term-1) + fib_head_recursion(term-2);
    }

    //Tail Recursion
    public static int fib_tail_recursion(int term, int a, int b){
        if(term == 0 ) // base condition
            return a;

        if(term == 1 ) // base condition
            return b;

        //tail recursion as last statement a standalone recursive function call
        return fib_tail_recursion(term-1, b, a +b );
    }


    public static void main(String[] args){
        System.out.println(fib_head_recursion(6));
        System.out.println(fib_tail_recursion(6, 0, 1));
    }
}
