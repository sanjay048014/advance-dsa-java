package advance.dsa.recursion;

public class Factorial {
    public static int factorial_head_recursion(int n){
        if(n == 0 ) // base condition
            return 1;

        //head recursion as last statement is not a standalone recursive function call
        return n * factorial_head_recursion(n-1) ;
    }

    public static int factorial_tail_recursion(int n, int result){
        if(n == 0 ) // base condition
            return result;

        //head recursion as last statement is not a standalone recursive function call
        return factorial_tail_recursion(n-1, result * n) ;
    }



    public static void main(String[] args){
        System.out.println(factorial_head_recursion(6));
        System.out.println(factorial_tail_recursion(6, 1));
    }
}
