package advance.dsa.search;// Java program for linear search

class LinearSearch
{

    public static boolean search(String arr[], String searchElement)
    {
        for (String element : arr) {
            if(element.equals(searchElement))
                return true;
        }
        return false;
    }


    // Driver code
    public static void main(String[] args)
    {
        String arr[] = { "abc", "SAM", "Silver", "Gold", "Bred" };
        String searchElement = "Silver";
        boolean isElementFound = search(arr, searchElement);

        // Function call
        System.out.println(searchElement + " is found in array: " + isElementFound);
    }
}

