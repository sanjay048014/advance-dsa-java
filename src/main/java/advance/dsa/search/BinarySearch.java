package advance.dsa.search;// Java program for Binary search, is applied for sorted data elements

class BinarySearch
{

    public static boolean search(String arr[], String searchElement, int startIdx, int endIdx)
    {
        if(startIdx > endIdx){ // base condition when there is no elements between startIdx and endIdx
            return false;
        }

        int mid = (startIdx + endIdx) /2;
        if(searchElement.compareTo(arr[mid]) == 0){ // search element equals arr[mid]
            return true;
        } else if (searchElement.compareTo(arr[mid]) < 0){ // search element less than arr[mid]
            return search(arr, searchElement, startIdx, mid - 1);
        } else {
            return search(arr, searchElement, mid + 1, endIdx); // search element greater than arr[mid]
        }
    }


    // Driver code
    public static void main(String[] args)
    {
        String arr[] = { "Abc", "Bred", "Gold", "Sam", "Silver"};
        String searchElement = "Silver";
        boolean isElementFound = search(arr, searchElement, 0, arr.length - 1);

        // Function call
        System.out.println(searchElement + " is found in array: " + isElementFound);
    }
}

